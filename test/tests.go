package main
import (
  "os"
  "fmt"
  "runtime"
 )
func main(){
  args := os.Args[1:]
  
  switch len(args) {
  case 0:
    all()
  case 1:
    testRoute(args[0])
  }
}

func createTrello(){
  trace()
  defer endTrace()
}

func createBoard(){
  trace()
  defer endTrace()
}

func createCard(){
  trace()
  defer endTrace()
}

func getTrello(){
  trace()
  defer endTrace()
}

func getBoard(){
  trace()
  defer endTrace()
}

func delTrello(){
  trace()
  defer endTrace()
}

func delBoard(){
  trace()
  defer endTrace()
}

func delCard(){
  trace()
  defer endTrace()
}

func updateCard(){
  trace()
  defer endTrace()
}

func updateTrello(){
  trace()
  defer endTrace()
}

func updateBoard(){
  trace()
  defer endTrace()
}

//----- Tooling ------//
func all(){
  var createFuncs = []func(){
    createTrello,
    createBoard,
    createCard,
  }

  var getFuncs = []func(){
    getTrello,
    getBoard,
  }

  var delFuncs = []func(){
    delTrello,
    delBoard,
    delCard,
  }

  var updateFuncs = []func(){
    updateTrello,
    updateBoard,
    updateCard,
  }

   all := []interface{}{
     createFuncs,
     getFuncs,
     delFuncs,
     updateFuncs,
   }

  for _,farr := range all{
    for _,f := range farr.([]func()){
      f()
    }
  }
}

func testRoute(route string){
  fmt.Println(route)
}

func trace() {
    pc := make([]uintptr, 10)  // at least 1 entry needed
    runtime.Callers(2, pc)
    f := runtime.FuncForPC(pc[0])
    name := fmt.Sprintf("%v",f.Name()[len("main."):])
    fmt.Printf("//---STARTING: %s",name)
    fmt.Printf("---//\n")
}

func endTrace() {
    fmt.Printf("//---Completed---//\n")
}
