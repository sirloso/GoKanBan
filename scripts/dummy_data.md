- DUMMY DATA
    - Projects:

        INSERT INTO projects ( mongo_project_id , trello_id ) VALUES

        ('abcde', 1);

        INSERT INTO projects ( mongo_project_id , trello_id ) VALUES

        ('abcde', 2);

    - Trellos:

        INSERT INTO trello (Title ,mongo_project_id, users ) VALUES ('workshop','abcde','{}');

        INSERT INTO trello (Title ,mongo_project_id, users ) VALUES ('sprint','abcde', '{}');

    - Boards:

        INSERT INTO boards ( title, description,due_date  , members, trello_id ) VALUES ('board1', 'testing first',DATE '1111-11-11', '{jeremy,ambrosia}', 1);

        INSERT INTO boards ( title, description  , due_date,trello_id ) VALUES ('2','ahhhhhh',DATE '1111-11-11', 1 );

        INSERT INTO boards ( title , description , due_date, members, trello_id ) VALUES ('sdf','',DATE '1111-11-11','{pablo}',1);

        INSERT INTO boards ( title ,description, due_date,trello_id ) VALUES ('sdf','',DATE '1111-11-11',2);

    - Cards:

        INSERT INTO cards ( title,description , due_date , assigned_to, completed , compensation , board_id , ASSIGNED ) VALUES( ' card1 ' , 1 ,DATE ' 2018-09-09' , 'olo' , false , 1 , 1 , '{-1,3}' );

        INSERT INTO cards ( title,description ,  assigned_to, completed , compensation , board_id , ASSIGNED ) VALUES(  ' card2 ' , 2  , 'john' , false , 11 , 2 , '{-1}' );

        INSERT INTO cards ( title , due_date , completed , compensation , board_id  ) VALUES( ' card3 '  ,DATE ' 2018-09-09'  , false , 31 , 1 );

        INSERT INTO cards ( title,description , due_date , assigned_to, completed , compensation , board_id , ASSIGNED ) VALUES(  ' card4 ' , 3 ,DATE ' 2018-09-09' , '3thirty' , false , 21 , 4 , '{123}' );

    - Text:

        INSERT INTO text ( Text ) VALUES ( 'only card in the world' );

        INSERT INTO text ( Text ) VALUES ( 'I am the true father of god' );

        INSERT INTO text ( Text ) VALUES (  'I am peter parker' );

    -Update Template:

    UPDATE trellos SET (  ) WHERE  trellos.ID= ($1);
    UPDATE boards SET (  ) WHERE  boards.ID= ($1);
    UPDATE cards SET (  ) WHERE  cards.ID= ($1);
    UPDATE text SET (  ) WHERE  text.ID= ($1);

    -Delete Templates:

    UPDATE trelos SET deleted = true WHERE  trellos.ID= ($1);
    UPDATE boards SET deleted = true WHERE  boards.ID= ($1);
    UPDATE cards SET  deleted = true WHERE  cards.ID= ($1);
    UPDATE text SET   deleted = true WHERE  text.ID= ($1);
