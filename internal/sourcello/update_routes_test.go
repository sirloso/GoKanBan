package sourcello
import (
  "testing"
  "fmt"
  "strconv"
)

var passedutil = true

//Returns generic array of structs
func getStruct() ( []struct{ oID int; v map[string]string; e bool } ) {
  StartDB()
  return []struct{
    oID int                     //ID of newly created Trello
    v map[string]string         //Map of all valued being updated
    e bool                      //Expected Value
  }{}
}

//Trello
func setupUpdateTrello() ( []struct{ oID int; v map[string]string; e bool } )  {
  vals := getStruct()

  script1 :=`INSERT INTO trello (title ,mongo_project_id) VALUES ('testTitleUpdate','testing1') RETURNING ID;`

  var ID1 int

  map1 := map[string]string{ "title" : "UPDATEDTITLE" }

  err := db.QueryRow(script1).Scan(&ID1); if err != nil { fmt.Println(err); panic("Error Creating Trello") }
  map1["ID"]=strconv.Itoa(ID1)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID1 , map1 , true,})

  return vals
}

func TestUtilUpdateTrello(t *testing.T){
  setup := setupUpdateTrello()
  for _,v := range setup{

    updated,err := UpdateTrello(v.v)

    if err!= nil { Check(err) }
    Assert(v.e,updated,t,&passedutil)
  }
  Passed()
  Cleanup("trello",setup)
}

//Board
func setupUpdateBoard() ( []struct{ oID int; v map[string]string; e bool } ) {
  vals := getStruct()

  script1:=`INSERT INTO boards ( title , description , trello_id ) VALUES ('t1','testing', 41) RETURNING ID;`
  script2:=`INSERT INTO boards ( title , description , members , trello_id ) VALUES ('t2','testing members updated','{carlos,santos}' , 420) RETURNING ID;`

  var (
    ID1 int
    ID2 int
  )

  map1 := map[string]string{ "title" : "Updated Board Title" , "description" : "I updated the description!",}
  map2 := map[string]string{ "members" : "{carlos,rullles}" }

  err := db.QueryRow(script1).Scan(&ID1); if err != nil { fmt.Println(err); panic("Error Creating Board") }
  map1["ID"]=strconv.Itoa(ID1)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID1 , map1 , true,})

  err = db.QueryRow(script2).Scan(&ID2); if err != nil { fmt.Println(err); panic("Error Creating Board") }
  map2["ID"]=strconv.Itoa(ID2)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID2 , map2 , true,})

  return vals
}


func TestUtilUpdateBoard(t *testing.T){
  setup := setupUpdateBoard()
  for _,v := range setup{

    updated,err := UpdateBoard(v.v)

    if err!= nil { Check(err) }
    Assert(v.e,updated,t,&passedutil)
  }
  Passed()
  Cleanup("boards",setup)
}

//Card
func setupUpdateCard() ( []struct{ oID int; v map[string]string; e bool } ) {
  vals := getStruct()

  script1:=`INSERT INTO cards ( title , description , assigned_to , completed , compensation , board_id) VALUES ('card1','112', 'john' , false, 123 , 666 ) RETURNING ID;`
  script2:=`INSERT INTO cards ( title , assigned ) VALUES ('c2','{pablo,escobar}' ) RETURNING ID;`

  var (
    ID1 int
    ID2 int
  )

  map1 := map[string]string{ "title" : "Updated Board Title" ,
     "assigned_to":"newyork","completed":"true", "compensation":"312",
   }
  map2 := map[string]string{ "assigned" : "{pablo,picaso}" }

  err := db.QueryRow(script1).Scan(&ID1); if err != nil { fmt.Println(err); panic("Error Creating Card") }
  map1["ID"]=strconv.Itoa(ID1)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID1 , map1 , true,})

  err = db.QueryRow(script2).Scan(&ID2); if err != nil { fmt.Println(err); panic("Error Creating Card") }
  map2["ID"]=strconv.Itoa(ID2)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID2 , map2 , true,})

  return vals
}

func TestUtilUpdateCard(t *testing.T) {
  setup := setupUpdateCard()
  for _,v := range setup{

    updated,err := UpdateCard(v.v)

    if err!= nil { Check(err) }
    Assert(v.e,updated,t,&passedutil)
  }
  Passed()
  Cleanup("cards",setup)
}

//Text
func setupUpdateText() ( []struct{ oID int; v map[string]string; e bool } ) {
  vals := getStruct()

  script1 :=`INSERT INTO text (text) VALUES ('testText') RETURNING ID;`

  var ID1 int

  map1 := map[string]string{ "text" : "UPDATEDTITLE" }

  err := db.QueryRow(script1).Scan(&ID1); if err != nil { fmt.Println(err); panic("Error Creating Text") }
  map1["ID"]=strconv.Itoa(ID1)
  vals = append(vals,struct{oID int; v map[string]string; e bool}{ID1 , map1 , true,})
  return vals
}

func TestUtilUpdateText(t *testing.T) {
  setup := setupUpdateText()
  for _,v := range setup{

    updated,err := UpdateText(v.v)

    if err!= nil { Check(err) }
    Assert(v.e,updated,t,&passedutil)
  }

  if passedutil { Passed() }
  Cleanup("text",setup)
}

func TestUtilAll(t *testing.T){
  TestAll(t,&passedutil)
}
