package sourcello

import (
  "database/sql"
  "fmt"
  //"log"
  "net/http"
  "os"
  "encoding/json"
  "strconv"
  "time"
  //"reflect"
  //"errors"
  "github.com/lib/pq"
)

var db *sql.DB

//Trello : Trello board to look up, which contains a collection of Boards
type Trello struct{
  ID int
  Title string
  Boards []Board
  Users []string
  MongoProjectID string
}

type Token struct {
  JWT string `json:"token"`
}

//NewTrello :
type NewTrello struct{
  Title string `json:"title"`
  Users []string `json:"users"`
  MongoProjectID string `json:"mongoProjectID"`
}

//Board : Collection of ...
type Board struct{
  ID int
  Cards []Card //Not part of SQL Schema used for returning
  Title string
  Description string
  DueDate time.Time
  Members []string
  TrelloID int
}

//NewBoard :
type NewBoard struct{
  Title string
  Description string
  DueDate string
  Members []string
  TrelloID int
}

//Card : Card collection
type Card struct{
  ID int
  Title string
  Description string
  DueDate time.Time
  AssignedTo string
  Completed bool
  Compensation int
  BoardID int
  Assigned []string
}

//NewCard :
type NewCard struct{
  Title string
  Description string
  DueDate string
  AssignedTo string
  Completed bool
  Compensation int
  BoardID int
  Assigned []string
}

//Text : Text obj
type Text struct{
  Body string
  //Same ID as Card ID
  ID int
}


const (
    dbhost = "DBHOST"
    dbport = "DBPORT"
    dbuser = "DBUSER"
    dbpass = "DBPASS"
    dbname = "DBNAME"
)

func getTrelloHandler(w http.ResponseWriter, r *http.Request) {
	id:= r.URL.Path[len("/sourcello/"):]
  //id,err := strconv.Atoi(idStr)

  trellos,err := getTrello(id)
  if err != nil {
        http.Error(w, err.Error(), 500)
    		return
 }

	fmt.Fprintf(w, string(trellos))
}

func getTrello(id string) ([]byte,error){
  trellos := []Trello{}
  err := getTrellos(id,&trellos);     if err!= nil{ return nil,err }
  err  = getBoards(&trellos); if err!= nil{ return nil,err }
  for _,trello := range trellos{
    err=getCards(&trello.Boards)
  }

  if err!= nil{ return nil,err }
  out, err := json.Marshal(trellos)
  if err != nil { return nil,err }
  return out,nil
}

func getTrellos(id string,arr *[]Trello) (error) {
  qtrello:= `SELECT * FROM trello WHERE trello.mongo_project_id = $1;`

  var(
    iD int
    title string
    mongo string
    users []string
  )

  rows, err := db.Query(qtrello,id)
  if err != nil {
    return err
  }

  for rows.Next(){
    rows.Scan(
      &iD,
      &title,
      &mongo,
      pq.Array(users),
    )
    *arr = append(*arr,Trello{ID:iD , Title: title , Boards: []Board{}, MongoProjectID: mongo , Users: users })
  }

  return nil
}

func getBoards(trellos *[]Trello) (error){
  for i,trello := range *trellos{
    qboards:= `SELECT * FROM boards WHERE boards.trello_id = $1;`

    var(
      id       int
      title    string
      desc     string
      dd       time.Time
      members  []string
      trelloid int
    )

    members = nil
    rows, err := db.Query(qboards,trello.ID)
    if err != nil {
      return err
    }

    for rows.Next(){
      err= rows.Scan(
        &id,
        &title,
        &desc,
        &dd,
        pq.Array(&members),
        &trelloid,
      )

      (*trellos)[i].Boards = append((*trellos)[i].Boards,
        Board{
          ID: id,
          Cards: []Card{},
          Title: title,
          Description: desc,
          DueDate: dd,
          Members: members,
          TrelloID: trelloid,
        })
    }
  }
  return nil
}

func getCards(boards *[]Board) (error){
  for i,board := range *boards{
    qcards:=`SELECT text,cards.id,title,description,due_date,assigned_to,completed,compensation,board_id,assigned FROM text INNER JOIN cards ON cards.description=text.id WHERE cards.board_id = $1 AND cards.deleted = false AND text.deleted = false`

    rows, err := db.Query(qcards,board.ID); if err != nil { fmt.Println("ERR",err); return err }
    defer rows.Close()
    var(
        text string
        id int
        title string
        dd time.Time
        assignedTo string
        completed bool
        compensation int
        BoardID int
        assigned []string
        desc string
      )
    for rows.Next(){
      err = rows.Scan(
        &text,
        &id,
        &title,
        &desc,
        &dd,
        &assignedTo,
        &completed,
        &compensation,
        &BoardID,
        pq.Array(&assigned),
      )

      (*boards)[i].Cards = append((*boards)[i].Cards,
        Card{
          ID: id,
          Title: title,
          Description: text,
          DueDate: dd,
          AssignedTo: assignedTo,
          Completed: completed,
          Compensation: compensation,
          BoardID: BoardID,
          Assigned: assigned,
        })
      }
    }
  return nil
}

type user struct{
  ID string
}



//Write check to reject if type isn' json
//Need to make sure that sql inserts are escaped and all nulls are set to ""
func postTrelloHandler(w http.ResponseWriter, r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data NewTrello
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  reciept, err := NewTrelloUtil(&data)
  if err != nil {
    fmt.Println("ERROR on recieving newTrello ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Created New Trello" , "Data" : { "ID" : %v } }`, reciept)
  fmt.Fprintf(w, string(out))
}

func newBoard(w http.ResponseWriter, r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data NewBoard
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  _, err = newBoardUtil(&data)
  if err != nil {
    fmt.Println("ERROR on recieving newBoard ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Created New Board" }`)
  fmt.Fprintf(w, string(out))
}

func newCard(w http.ResponseWriter, r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data NewCard
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  _, err = newCardUtil(&data)
  if err != nil {
    fmt.Println("ERROR on recieving newCard ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Created New Card" }`)
  fmt.Fprintf(w, string(out))
}

//New
func NewTrelloUtil(trello *NewTrello) ( int , error ) {
  if trello.Users == nil { trello.Users = []string{} }
  newScript := `INSERT INTO trello (Title , mongo_project_id , users) VALUES ( $1 , $2 , $3) RETURNING ID;`
  var ID int
  err := db.QueryRow( newScript,trello.Title , trello.MongoProjectID , pq.Array(trello.Users) ).Scan(&ID)
  if err!= nil {
    fmt.Println("ERROR creating new trello",err);
    return -1 , err
  }
  return ID,nil
}

func newBoardUtil(board *NewBoard) ( bool , error ){
  //if board.TrelloID ==  { return -1, err.Error(" No Trello ID Specified ") }
  if board.Members == nil { board.Members = []string{} }
  newScript := `INSERT INTO boards ( title , description,due_date , members , trello_id )
  VALUES ( $1 , $2 , $3 , $4 , $5 );`

  date,err := time.Parse("2018-01-01",board.DueDate)

  _,err = db.Exec( newScript, board.Title , board.Description ,date, pq.Array(board.Members) ,board.TrelloID  )
  if err!= nil {
    fmt.Println("ERROR creating new Board",err);
    return false , err
  }

  return true , nil
}

func newCardUtil(card *NewCard) ( bool , error ) {
  if card.Assigned == nil { card.Assigned = []string{} }
  newScript := `INSERT INTO cards ( title, description , due_date , assigned_to, completed , compensation , board_id , ASSIGNED )
  VALUES ( $1 , $2 , $3 , $4 , $5 , $6 , $7 , $8 );`

  textID , err := newTextUtil( card.Description )
  if err != nil {
    fmt.Println("ERROR creating new Text",err);
    return false , err
  }

  date,err := time.Parse("2018-01-01",card.DueDate)
  _,err = db.Exec(
    newScript, card.Title , textID ,
    date , card.AssignedTo , card.Completed ,
    card.Compensation , card.BoardID , pq.Array(card.Assigned),
  )

  if err != nil {
    fmt.Println("ERROR creating new Card",err);
    //delText(ID)
    return false , err
  }

  return true , nil
}

func newTextUtil( body string ) ( int, error ) {
  newScript := `INSERT INTO text ( Text ) VALUES ( $1 )  RETURNING ID;`
  var ID int
  err := db.QueryRow( newScript , body ).Scan(&ID)
  if err != nil{
    fmt.Println( "ERROR Creating New Text" , err )
    return -1 , err
  }

  return ID , nil
}

//UpdateTrello : Updates Trello Entry
func UpdateTrello(info map[string]string) ( bool , error ) {
  id,err:=strconv.Atoi(info["ID"])
  script,err := UpdateScript(Trello{},id,info);
  if err!= nil {
    fmt.Println("ERROR WITH UPDATER",err)
    return false,err
  }
  _,err = db.Exec( script )
  if err!= nil {
    fmt.Println("ERROR Updating Trello",err);
    return false , err
  }

  return true , nil
}


//UpdateBoard : Updates Board Entry
func UpdateBoard(info map[string]string) ( bool , error ) {
  id,err:=strconv.Atoi(info["ID"])
  script,err := UpdateScript(Board{},id,info);
  if err!= nil {
    fmt.Println("ERROR WITH UPDATER",err)
    return false,err
  }

  _,err = db.Exec( script )
  if err!= nil {
    fmt.Println("ERROR Updating Board",err);
    return false , err
  }

  return true , nil
}

//UpdateCard : Updates Card Entry
func UpdateCard(info map[string]string) ( bool , error ) {
  id,err:=strconv.Atoi(info["ID"])
  script,err := UpdateScript(Card{},id,info);
  if err!= nil {
    fmt.Println("ERROR WITH UPDATER",err)
    return false,err
  }

  _,err = db.Exec( script )
  if err!= nil {
    fmt.Println("ERROR Updating Card",err);
    return false , err
  }

  return true , nil
}

//UpdateText : Updates Text Entry
func UpdateText(info map[string]string) ( bool , error ) {
  id,err:=strconv.Atoi(info["ID"])
  script,err := UpdateScript(Text{},id,info)
  if err!= nil {
    fmt.Println("ERROR WITH UPDATER",err)
    return false,err
  }

  _,err = db.Exec( script )
  if err!= nil {
    fmt.Println("ERROR Updating Text",err);
    return false , err
  }

  return true , nil
}

//Deletes
func DelTrelloUtil(ID int) ( bool , error ) {
  delScript := `UPDATE trelos SET deleted = true WHERE  trellos.ID= ($1);`
  _,err := db.Exec(delScript ,ID)
  if err != nil{
    fmt.Println("ERROR Deleting Trello with ID: ",ID)
    return false,err
  }
  return true , nil
}

func DelBoardUtil(ID int) ( bool , error ) {
  delScript := `UPDATE boards SET deleted = true WHERE  boards.ID= ($1);`
  _,err := db.Exec(delScript ,ID)
  if err != nil{
    fmt.Println("ERROR Deleting Board with ID: ",ID)
    return false,err
  }
  return true , nil
}

func DelCardUtil(ID int) ( bool , error ) {
  delScript := `UPDATE cards SET deleted = true WHERE  cards.ID= ($1);`
  _,err := db.Exec(delScript , ID)
  if err != nil{
    fmt.Println("ERROR Deleting Card with ID: ",ID)
    return false,err
  }
  return true , nil
}

func DelTextUtil(ID int) ( bool , error ) {
  delScript := `UPDATE text SET deleted = true WHERE  text.ID= ($1);`
  _,err := db.Exec(delScript , ID)
  if err != nil{
    fmt.Println("ERROR Deleting Text with ID: ",ID)
    return false,err
  }
  return true , nil
}

//DelTrello : Sets Trello to Deleted=true
func DelTrello(w http.ResponseWriter , r *http.Request){
    decoder := json.NewDecoder(r.Body)

    var data struct { ID int }
    err := decoder.Decode(&data)
    if err != nil {
      panic(err)
    }
    _, err = DelTrelloUtil(data.ID)
    if err != nil {
      fmt.Println("ERROR on deleting newTrello ",err)
      http.Error(w,"Internal Error", 500);
      return
    }
    out := fmt.Sprintf(`{ "Message" : "Successfully Deleted Trello" }`)
    fmt.Fprintf(w, string(out))

}

//DelBoard : Sets Board to Deleted=true
func DelBoard(w http.ResponseWriter , r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data struct { ID int }
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  _, err = DelBoardUtil(data.ID)
  if err != nil {
    fmt.Println("ERROR on deleting Board ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Deleted Board" }`)
  fmt.Fprintf(w, string(out))
}

//DelCard : Sets Card to Deleted=true
func DelCard(w http.ResponseWriter , r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data struct { ID int }
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  _, err = DelCardUtil(data.ID)
  if err != nil {
    fmt.Println("ERROR on deleting Card ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Deleted Card" }`)
  fmt.Fprintf(w, string(out))
}

//DelText : Sets Text to Deleted=true
func DelText(w http.ResponseWriter , r *http.Request){
  decoder := json.NewDecoder(r.Body)

  var data struct { ID int }
  err := decoder.Decode(&data)
  if err != nil {
    panic(err)
  }
  _, err = DelTextUtil(data.ID)
  if err != nil {
    fmt.Println("ERROR on deleting Text ",err)
    http.Error(w,"Internal Error", 500);
    return
  }
  out := fmt.Sprintf(`{ "Message" : "Successfully Deleted Text" }`)
  fmt.Fprintf(w, string(out))
}

//UpdateScript : Creates a new instance of an Update class
//class generates the sql script to update the new values
func UpdateScript( i interface{} , id int , info map[string]string ) (string,error){
  updater := Updater{}
  var err error
  err = updater.ChooseType(i,id);   if err !=nil { return "-1",err }
  err = updater.CreateScript(info); if err !=nil { return "-1",err }
  updater.Finalize()

  return updater.Script,nil
}

func initDb() {
    config := dbConfig()
    var err error
    psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
        "password=%s dbname=%s sslmode=disable",
        config[dbhost], config[dbport],
        config[dbuser], config[dbpass], config[dbname])

    db, err = sql.Open("postgres", psqlInfo)
    if err != nil {
        panic(err)
    }
    err = db.Ping()
    if err != nil {
        panic(err)
    }
    fmt.Println("Successfully connected!")
}

func dbConfig() map[string]string {
    conf := make(map[string]string)
    host, ok := os.LookupEnv(dbhost)
    if !ok {
        panic("DBHOST environment variable required but not set")
    }
    port, ok := os.LookupEnv(dbport)
    if !ok {
        panic("DBPORT environment variable required but not set")
    }
    user, ok := os.LookupEnv(dbuser)
    if !ok {
        panic("DBUSER environment variable required but not set")
    }
    password, ok := os.LookupEnv(dbpass)
    if !ok {
        panic("DBPASS environment variable required but not set")
    }
    name, ok := os.LookupEnv(dbname)
    if !ok {
        panic("DBNAME environment variable required but not set")
    }
    conf[dbhost] = host
    conf[dbport] = port
    conf[dbuser] = user
    conf[dbpass] = password
    conf[dbname] = name
    return conf
}

//StartDB : sdf
func StartDB() {
  initDb()
}
