package sourcello

import (
  "fmt"
  //"log"
  "net/http"
  "strconv"
  "path"
  "strings"
  "os"
  "io/ioutil"
  "encoding/json"
  jwt "github.com/dgrijalva/jwt-go"
)

var (
  _serverjs string
  _secret string
)

type App struct {
    // We could use http.Handler as a type here; using the specific type has
    // the advantage that static analysis tools can link directly from
    // h.SourcelloHandler.ServeHTTP to the correct definition. The disadvantage is
    // that we have slightly stronger coupling. Do the tradeoff yourself.
    SourcelloHandler *SourcelloHandler
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func ShiftPath(p string) (head, tail string) {
    p = path.Clean("/" + p)
    i := strings.Index(p[1:], "/") + 1
    if i <= 0 {
        return p[1:], "/"
    }
    return p[1:i], p[i:]
}

func (h *App) ServeHTTP(res http.ResponseWriter, req *http.Request) {
    var head string
    head, req.URL.Path = ShiftPath(req.URL.Path)
    if head == "delBoard" {
      decoder := json.NewDecoder(req.Body)

      var data struct{ ID int }
      err := decoder.Decode(&data)
      if err != nil {
        panic(err)
      }
      _, err = DelBoardUtil(data.ID)
      if err != nil {
        fmt.Println("ERROR on recieving delete ",err)
        http.Error(res,"Internal Error", 500);
        return
      }
      out := fmt.Sprintf(`{ "Message" : "Successfully deleted thing" }` )
      fmt.Fprintf(res, string(out))
    }

    if head == "delText" {
      decoder := json.NewDecoder(req.Body)

      var data struct{ ID int }
      err := decoder.Decode(&data)
      if err != nil {
        panic(err)
      }
      _, err = DelTextUtil(data.ID)
      if err != nil {
        fmt.Println("ERROR on recieving delete ",err)
        http.Error(res,"Internal Error", 500);
        return
      }
      out := fmt.Sprintf(`{ "Message" : "Successfully deleted thing" }` )
      fmt.Fprintf(res, string(out))
    }

    if head == "delCard" {
      decoder := json.NewDecoder(req.Body)

      var data struct{ ID int }
      err := decoder.Decode(&data)
      if err != nil {
        panic(err)
      }
      _, err = DelCardUtil(data.ID)
      if err != nil {
        fmt.Println("ERROR on recieving delete ",err)
        http.Error(res,"Internal Error", 500);
        return
      }
      out := fmt.Sprintf(`{ "Message" : "Successfully deleted thing" }` )
      fmt.Fprintf(res, string(out))
    }

    if head != "sourcello" {
        http.Error(res, "Not Found", http.StatusNotFound)
        return
    }
    switch req.Method {
      case "GET":
          h.SourcelloHandler.handleGet(res, req)
      // case "PUT":
      //     h.SourcelloHandler.handlePut(res, req)
      case "POST":
        h.SourcelloHandler.handlePost(res, req)
      // case "DELETE":
      //   h.SourcelloHandler.handleDelete(res, req)
      default:
          http.Error(res, "Only GET POST PUT and DELETE are allowed", http.StatusMethodNotAllowed)
    }
}

type SourcelloHandler struct {
}


func (h *SourcelloHandler) handleGet(res http.ResponseWriter, req *http.Request) {
    var sub, idStr string
    sub, req.URL.Path = ShiftPath(req.URL.Path) // "trello" | "project" | "board" | "card"
    idStr, req.URL.Path = ShiftPath(req.URL.Path) // "id"
    id, err := strconv.Atoi(idStr)
    if err != nil {
        http.Error(res, fmt.Sprintf("Invalid id %q", sub), http.StatusBadRequest)
        return
    }
    switch sub {
      case "trello": //get a specific trello
        callUtil(res, id, GetTrello)
        return
      // case "project": //get all trellos for a given project
      //   callUtil(res, id, GetProject)
      //   return
      // case "board": //get a specific trello board
      //   callUtil(res, id, GetBoard)
      //   return
      // case "card": //get a specific card
      //   callUtil(res, id, GetCard)
      //   return
      default:
        http.Error(res, "Not Found", http.StatusNotFound)
        return
    }
}


func (h *SourcelloHandler) handlePost(res http.ResponseWriter, req *http.Request) {
    var sub string
    sub, req.URL.Path = ShiftPath(req.URL.Path) // "trello" | "project" | "board" | "card"
    body, err := ioutil.ReadAll(req.Body)
    if err != nil {
      http.Error(res, fmt.Sprintf("%v", err), http.StatusBadRequest)
      return
    }

    token, err := parseToken(body)
    if err!=nil {
      http.Error(res, fmt.Sprintf("%v", err), http.StatusUnauthorized)
      return
    }
    switch sub {
      case "trello": //Create a new trello
        var t NewTrello
        err := json.Unmarshal(body, &t)
        if err != nil {
          http.Error(res, fmt.Sprintf("%v", err), http.StatusBadRequest)
          return
        }
        userId := token["id"].(string)
        if err != nil {
          http.Error(res, fmt.Sprintf("%v", err), http.StatusBadRequest)
          return
        }
        authorized := verifyCreator(userId, t.MongoProjectID)
        if !authorized {
          http.Error(res, fmt.Sprintf("You cannot create trellos for this project"), http.StatusUnauthorized)
          return
        }
        tid, err := NewTrelloUtil(&t)
        if err != nil {
          http.Error(res, fmt.Sprintf("%v", err), http.StatusInternalServerError)
          return
        }
        fmt.Println(tid)
        return
      // case "board":
      //   callUtil(res, id, "GetBoard")
      //   return
      // case "card":
      //   callUtil(res, id, "GetCard")
      //   return
      // case "assign":
      //   callUtil(res, id, "GetProject")
      //   return
      default:
        http.Error(res, "Not Found", http.StatusNotFound)
        return
    }
}

// func CreateTrello(req *http.Request) (Trello, error) {
//   var t NewTrello
//   decoder := json.NewDecoder(req.Body)
//   err := decoder.Decode(&t)
//   if err != nil {
//     http.Error(res, fmt.Sprintf("%v", err), http.StatusBadRequest)
//     return
//   }
//   fmt.Printf("%+v\n", t)
// }

func verifyCreator(mongoUserID string, mongoProjectID string) (bool) {
  url := _serverjs + "/api/repos/verify" + "?user=" + mongoUserID + "&project=" + mongoProjectID
  res, err := http.DefaultClient.Get(url)
  if err != nil {return false}
  if res.StatusCode == 200 {
    return true
  }
  return false
}


// func parseBody(req *http.Request) (map[string]interface{}, error) {
//   var t map[string]interface{}
//
//   decoder := json.NewDecoder(req.Body)
//   err := decoder.Decode(&t)
//
//   if err != nil {
//     return nil, err
//   }
//   return t, err
// }


func parseToken(body []byte) (jwt.MapClaims, error){
  var t Token
  err := json.Unmarshal(body, &t)
  if err != nil {
    return nil, fmt.Errorf("Unable to parse token")
  }
  token, err := jwt.Parse(t.JWT, func(token *jwt.Token) (interface{}, error) {
      // Don't forget to validate the alg is what you expect:
      if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
          return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
      }

      return []byte(_secret), nil
  })

  if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
      return claims, nil
  } else {
      return nil, err
  }
}



type xxx func(int) (string, error)

func callUtil(res http.ResponseWriter, id int, util xxx) {
  jsonStr, err := util(id)
  if err != nil {
      http.Error(res, fmt.Sprintf("Error getting trello: %s", err), http.StatusInternalServerError)
      return
  }
  fmt.Fprintf(res, "%s", jsonStr)
}


//NOTE: for testing only
func GetTrello(id int) (string, error) {
  jsonStr := strconv.Itoa(id)
  return  jsonStr, nil
}

func env() {
  sec, ok := os.LookupEnv("SECRET")
  if !ok {
      panic("SECRET environment variable required but not set")
  }
  ser, ok := os.LookupEnv("SERVERJS")
  if !ok {
      panic("SECRET environment variable required but not set")
  }
  _secret = sec
  _serverjs = ser
  return
}
//Start : sdf
func Start() {
  env()
  a := &App{
      SourcelloHandler: new(SourcelloHandler),
  }
	log.Fatal(http.ListenAndServe(":8080", a))
  StartDB()
}
