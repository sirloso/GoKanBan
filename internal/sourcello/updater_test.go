package sourcello

import (
  "testing"
  "fmt"
  "strconv"
  "errors"
  . "github.com/logrusorgru/aurora"
  )

var passedupdater = true
var passedUpdater = &passedupdater
//TestChooseType : Test the switching of type in updater
func TestUpdaterChooseType(t *testing.T){
  //Need to work out timing of parallel tests in order to print things correctly
  //t.Parallel()
  //setup
  var vals = []struct{
    t interface{}   //type
    e string        //Expected type
    id int          //Expected id
  }{
    { Trello{} , "trello" , 666 },
    { Card{}   , "card"   , 665 },
    { Text{}   , "text"   , 664 },
    { Board{}  , "board"  , 663 },
  }
  //New Updater
  u := Updater{}

  for _,v := range vals{
    u.ChooseType(v.t,v.id)
    if v.e  != u.Type{ Format(v.e,u.Type,t,passedUpdater) }
    id,err :=strconv.Atoi(u.ID)
    if err!= nil { Format(strconv.Itoa(v.id),err.Error(),t,passedUpdater) }
    if v.id != id{ Format(strconv.Itoa(v.id),u.ID,t,passedUpdater) }
  }
  fmt.Println(Green("Passed Test"))
}

func setupCreateScript() ([]struct{ u Updater; t map[string]string; e string }){
  //Create scripts first
  var a = make(map[string]string)
  var b = make(map[string]string)
  var c = make(map[string]string)

  //Set IDs
  a["ID"]="897"
  b["ID"]="898"

  //Set Updates
  a["title"]="Orange 987"
  a["description"]="tilemeupbb"

  b["ID"]="898"
  //setup
  var vals = []struct{
    u Updater //object
    t map[string]string   //type
    e string   //Expected
  }{
    { Updater{ Type:"trello"} , a , "description='tilemeupbb',title='Orange 987'" },
    { Updater{ Type:"board"}  , b , errors.New("No Updates Present").Error()              },
    { Updater{ Type:"Card"}   , c , errors.New("No ID Included, unable to process request").Error() },
  }
  return vals
}

//TestCreateScript : Tests Create Script funciton
func TestUpdaterCreateScript(t *testing.T){
  vals := setupCreateScript()
  for _,v := range vals{
    err := v.u.CreateScript(v.t)
    if err!= nil && err.Error()!=v.e{
      Format(v.e,err.Error(),t,passedUpdater)
    }
    if err==nil && v.e!=v.u.Updates{
      Format(v.e,v.u.Updates,t,passedUpdater);
    }
  }
  fmt.Println(Green("Passed Test"))
}

func setupTestFinalize() ([]struct{ u Updater; e string } ){
  //setup
  var vals = []struct{
    u Updater   //type
    e string   //Expected
  }{
    {
      Updater{
        ID: "666",
        Type: "trello",
        Updates: "title='NONONO'",
      },
      "UPDATE trello SET title='NONONO' WHERE trello.ID=666;",
    },
    {
      Updater{
        Type:"board" ,
        ID:"668",
        Updates: "description='updated',members='{jen,john}'",
      },
      "UPDATE board SET description='updated',members='{jen,john}' WHERE board.ID=668;",
    },
  }
  return vals
}

//TestFinalize : Test the finalization method of Updater
func TestUpdaterFinalize(t *testing.T){
  vals := setupTestFinalize()
  for _,v := range vals{
    v.u.Finalize()
    if v.u.Script!=v.e{
      Format(v.e,v.u.Script,t,passedUpdater)
      return
    }
  }
  fmt.Println(Green("Passed Test"))
}

//TestIntegration : Runs through entire suite of Updater
func TestUpdaterIntegration(t *testing.T){
  script := "UPDATE text SET Body='OH NO SOS WE'RE CRASHING' WHERE text.ID=666;"
  u := Updater{}
  u.ChooseType(
    Text{
      Body:"Hello from uphere in space!",
      ID: 666,
    },
    666,
  )

  m := make(map[string]string)
  m["ID"] = "666"
  m["Body"] = "OH NO SOS WE'RE CRASHING"

  u.CreateScript(m)
  u.Finalize()
  if u.Script!=script{
    Format(script,u.Script,t,passedUpdater)
    return
  }
  fmt.Println(Green("PASSED TEST"))
}

func TestUpdaterAll(t *testing.T){
  TestAll(t,passedUpdater)
}
