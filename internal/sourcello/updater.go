package sourcello
import (
  "bytes"
  "errors"
  "fmt"
  "strconv"
  "sort"
 )
//Updater :
type Updater struct{
  ID string
  Type string
  Updates string
  Script string
}

//ChooseType : function readies the Table name to build the query
func (u *Updater) ChooseType(i interface{},id int) error {
  switch t := i.(type) {
    case Trello:
      u.Type = "trello"
    case Board:
      u.Type = "boards"
    case Card:
      u.Type = "cards"
    case Text:
      u.Type = "text"
    default:
      u.Type = t.(string)
      msg := "Unable to convert type into a source type.\n Found type: %v"
      msg = fmt.Sprintf(msg,u.Type)
      return errors.New(msg)
  }
  u.ID = strconv.Itoa(id)
  return nil
}

func wrap(v string) (string){
  var s bytes.Buffer
  s.WriteString("'")
  s.WriteString(v)
  s.WriteString("'")
  return s.String()
}


//CreateScript : Takes input of values to be updated and writes db script to itself
func (u *Updater) CreateScript( v map[string]string ) error {
  //NOT NEEDED IF Checked in ChooseType!
  if v["ID"] == "" { return errors.New("No ID Included, unable to process request") }
  if len(v) == 1 { return errors.New("No Updates Present") }
  var updates bytes.Buffer

  delete( v , "ID")
  l := len(v)
  keys := make( []string , 0 , l )
  for k := range v { keys = append(keys, k) }
  sort.Strings(keys)

  i := 1
  for _,k := range keys{
    updates.WriteString(k)
    updates.WriteString("=")
    updates.WriteString(wrap(v[k]))
    if i != l {
      updates.WriteString(",")
    }
    i++
  }

  u.Updates = updates.String()

  return nil
}

//Finalize takes all its inputs and constructs the script to be executed
func (u *Updater) Finalize(){
  tmp := "UPDATE %v SET %v WHERE %v.ID=%v;"
  u.Script= fmt.Sprintf(tmp,u.Type,u.Updates,u.Type,u.ID)
}
