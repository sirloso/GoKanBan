package sourcello

import (
  "testing"
  "fmt"
  . "github.com/logrusorgru/aurora"
  "strconv"
)

//Check : accepts an error and then takes appropriate action
func Check( err error ){
    fmt.Println(Sprintf(Red("ERROR FOUND:\n %v"),err))
    return
}

//Cleanup : Deletes all new Records Created
func Cleanup( tipe string , objects []struct{ oID int; v map[string]string; e bool } ){
  defer db.Close()
  for _,o := range objects{
    script:= `DELETE FROM %v WHERE ID = %v;`
    script = fmt.Sprintf(script,tipe,strconv.Itoa(o.oID))
    _,err := db.Exec(script); if err != nil { fmt.Println( "Didn't delete record:",o.oID,err )}
  }
}

//NotEqual : Panics if types are iniquivalent in Assert
func NotEqual(a interface{} , b interface{}){
  fmt.Println("A: ",a,"B: ",b)
  panic("A&B TYPES NOT EQUIVALENT")
}

//Assert : Makes sure two things are equal
func Assert( a interface{} , b interface{} , t *testing.T , p *bool){
    switch a.(type){
      case string:
        a2 := a.(string)
        b2,ok := b.(string); if !ok{ NotEqual(a,b) }
        if a2!=b2 { Format(a2,b2,t,p) }
      case bool:
        ab := strconv.FormatBool(a.(bool))
        bbc,ok := b.(bool); if !ok{ NotEqual(a,b) }
        bb := strconv.FormatBool(bbc)
        if ab!=bb { Format(ab,bb,t,p) }
    }
}

//Passed : prints "Passed Test" in green
func Passed(){
  fmt.Println(Green("PASSED TEST"))
}

//Format an error text
func Format(expected string , gotten string , t *testing.T ,passed *bool){
  fmt.Printf(Sprintf(Red("---Expected output: %v - Given output %v---\n"),expected,gotten))
  *passed=false
  t.Fail()
  return
}

//TestAll :
func TestAll(t *testing.T,passed *bool){
  if *passed {
      fmt.Println(Green("\n==================\n ALL TESTS PASSED\n==================\n"))
    }else{
      fmt.Println(Red("\n===========================\n FAILED ONE OR MORE TESTS\n==========================="))
    }
}
